package Aircraft;

import Coordinates.Coordinates;

public class Aircraft {
	
	public long id;
	public String name;
	public long idCounter;
	Coordinates Coordinates;
	public Aircraft(long id, String name, long idCounter,Coordinates coordinates) {
		this.id = id;
		this.name = name;
		this.idCounter = idCounter;
		Coordinates = coordinates;
	}
	
	public Aircraft() {
		super();
		// TODO Auto-generated constructor stub
	}
	public long nextId(){
		return (id+ 1);
	}	
}
