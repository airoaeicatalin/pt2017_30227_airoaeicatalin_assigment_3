package Flyable;

import Tower.WeatherTower;

/**
 * @author Dragos Boar
 *
 */
public interface Flyable {
	void updateCondition();
	void registerTower(WeatherTower WeatherTower);
}
