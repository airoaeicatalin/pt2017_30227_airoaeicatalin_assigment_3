package Main;

import java.awt.List;
import java.io.BufferedReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import Flyable.Flyable;
import Aircraft.Aircraft;
import Coordinates.Coordinates;
import Tower.WeatherTower;
import Tower.WeatherProvider;
import Factory.AircraftFactory;
public class Main {
	public static String textLog = "";
	public static boolean isAlphaNumeric(String s){
	    String pattern= "^[a-zA-Z 0-9]*$";
	    return s.matches(pattern);
	    
	}
	synchronized public static void setLog(String s){
		textLog = s;
	}
	private static ArrayList<String> addresses = new ArrayList<String>();
	 public static  int changeWeather(int[] array) {
	        int rnd = new Random().nextInt(array.length);
	        return array[rnd];
	    }
		public static String getWeather(Coordinates cord)
		{
			int latitudine[] = {1,5,10};
			int longitudine[] = {1,2,5,10};
			int height[] = {2,-7};
			if(cord.height + changeWeather(height) == cord.height + 2){
				if(cord.latitudine + changeWeather(latitudine)  == cord.latitudine + 10)
					return "SUN";
				if(cord.longitudine + changeWeather(longitudine) == (cord.longitudine + 10))
						return "SUN";
			}else if(cord.height + changeWeather(height) == cord.height -7)
				return "SNOW";
			else if(cord.height + changeWeather(height) == cord.height - 12)
				return "SNOW";
			else if(cord.height + changeWeather(height) == cord.height + 4){
				if(cord.latitudine + changeWeather(latitudine) == cord.latitudine + 2)
					return "SUN";
			}
			else if(cord.height + changeWeather(height) == cord.height - 5)
				return "RAIN";
			else if(cord.height + changeWeather(height) == cord.height - 3)
				return "FOG";
			if(cord.latitudine + changeWeather(latitudine) == cord.latitudine + 1)
				return "FOG";
			else if(cord.latitudine + changeWeather(latitudine) == cord.latitudine + 5)
				return "RAIN";
			if(cord.longitudine + changeWeather(longitudine) == cord.longitudine + 5)
				return "RAIN";
			else if(cord.longitudine + changeWeather(longitudine) == cord.longitudine + 1)
				return "FOG";
			return "NEINREGISTRAT";
		}
	public static void main(String [] args) throws IOException
	{

		BufferedReader br = new BufferedReader(new FileReader("file.txt"));//citesc din fisier
		try {
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();
		    long id = 1;
		    long idc = 0;
		    
		    int timeSimulation = Integer.parseInt(line); // Extrag timpul de simulare si in transform in int
		    System.out.println(timeSimulation);
		    line = br.readLine(); // Trec de timpul de simulare in fisier
		    while (line != null) {
		    	String split[] = line.split(" ");
		    	//System.out.println(split[0]);
		    	if(!split[0].equals("Baloon") && !split[0].equals("JetPlane") && !split[0].equals("Helicopter"))
		    		//Verificam daca e una din cele 3
		    		System.out.println("ERROR");
		    	int longitudine = Integer.parseInt(split[2]);
		    	int latitudine = Integer.parseInt(split[3]);
		    	if( longitudine < 0 ||  latitudine < 0)
		    		System.out.println("ERROR");
		    	int hight = Integer.parseInt(split[4]);
		    	if(hight > 100){
		    		hight = 100;
		    	}
		    	Coordinates c = new Coordinates(longitudine,latitudine,hight);
		    	Aircraft a = new Aircraft(id,split[0],idc,c);
		    	if(split[0].equals("Baloon"))
		    			setLog("Tower says: Baloon#"+split[1]+"("+id+")" +" registered to weather tower.");
		    	else if(split[0].equals("JetPlane"))
		    			setLog("Tower says: JetPlane#"+split[1]+"("+id+")" +" registered to weather tower.");
		    	else
		    		setLog("Tower says: Helicopter#"+split[1]+"("+id+")" +" registered to weather tower.");
		    	//System.out.println(textLog);
		    	addresses.add(split[0]);
		    	addresses.add(split[1]);
		    	addresses.add(split[2]);
		    	addresses.add(split[3]);
		    	addresses.add(split[4]);
		    	//System.out.println(addresses);
		    	id = a.nextId();
		        sb.append(line);
		        sb.append(System.lineSeparator());
		        line = br.readLine();

		    }
		    int index = 0;
		    while(timeSimulation != 0)
		    {
		    	Coordinates c = new Coordinates(Integer.parseInt(addresses.get(index+2)),Integer.parseInt(addresses.get(index+3)),Integer.parseInt(addresses.get(index+4)));	
		    	System.out.println(getWeather(c));
		    	timeSimulation--;
		    }
		    String everything = sb.toString();
			//System.out.println(everything);	//Afisez tot continutul fisierului;
		} finally {
		    br.close();
		}
	
	}
}
