package Coordinates;

public class Coordinates {
	public int longitudine;
	public int latitudine;
	public int height;
	public int getLongitudine() {
		return longitudine;
	}
	public void setLongitudine(int longitudine) {
		this.longitudine = longitudine;
	}
	public int getLatitudine() {
		return latitudine;
	}
	public void setLatitudine(int latitudine) {
		this.latitudine = latitudine;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public Coordinates(int longitudine, int latitudine, int height) {
		super();
		this.longitudine = longitudine;
		this.latitudine = latitudine;
		this.height = height;
	}
	public Coordinates() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
