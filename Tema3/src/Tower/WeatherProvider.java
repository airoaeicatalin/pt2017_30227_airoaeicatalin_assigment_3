package Tower;

import java.util.Random;
import Aircraft.Aircraft;

import Coordinates.Coordinates;

public class WeatherProvider {
	private WeatherProvider weatherProvider;
	public String[] weather;
	
	public WeatherProvider getWeatherProvider() {
		return weatherProvider;
	}
	public void setWeatherProvider(WeatherProvider weatherProvider) {
		this.weatherProvider = weatherProvider;
	}
	public WeatherProvider() {
		super();
		// TODO Auto-generated constructor stub
	}
	 public static  int changeWeather(int[] array) {
	        int rnd = new Random().nextInt(array.length);
	        return array[rnd];
	    }

	public String currentWeahterProvider(Coordinates cord)
	{
		int latitudine[] = {1,5,10};
		int longitudine[] = {1,2,5,10};
		int height[] = {2,-7};
		if(cord.height + changeWeather(height) == cord.height + 2){
			if(cord.latitudine + changeWeather(latitudine)  == cord.latitudine + 10)
				return "SUN";
			if(cord.longitudine + changeWeather(longitudine) == (cord.longitudine + 10))
					return "SUN";
		}else if(cord.height + changeWeather(height) == cord.height -7)
			return "SNOW";
		else if(cord.height + changeWeather(height) == cord.height - 12)
			return "SNOW";
		else if(cord.height + changeWeather(height) == cord.height + 4){
			if(cord.latitudine + changeWeather(latitudine) == cord.latitudine + 2)
				return "SUN";
		}
		else if(cord.height + changeWeather(height) == cord.height - 5)
			return "RAIN";
		else if(cord.height + changeWeather(height) == cord.height - 3)
			return "FOG";
		if(cord.latitudine + changeWeather(latitudine) == cord.latitudine + 1)
			return "FOG";
		else if(cord.latitudine + changeWeather(latitudine) == cord.latitudine + 5)
			return "RAIN";
		if(cord.longitudine + changeWeather(longitudine) == cord.longitudine + 5)
			return "RAIN";
		else if(cord.longitudine + changeWeather(longitudine) == cord.longitudine + 1)
			return "FOG";
		return "NEINREGISTRAT";
	}
}

